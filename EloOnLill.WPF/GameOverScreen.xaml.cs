﻿using EloOnLill.Domain;
using EloOnLill.WPF.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EloOnLill.WPF
{
    /// <summary>
    /// Interaction logic for GameOverScreen.xaml
    /// </summary>
    public partial class GameOverScreen : Window
    {
        private MainWindow_VM _vm;
          
        
        public GameOverScreen(CurrentGame x)
        {
            _vm = new MainWindow_VM(x);
            InitializeComponent();
            this.DataContext = _vm;
            //this.Loaded += MainWindow_Loaded;
        }
    }
}
