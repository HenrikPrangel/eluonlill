﻿using EloOnLill.Domain;
using EloOnLill.Services;
using EloOnLill.WPF.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EloOnLill.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int counter = 0;
        private MainWindow_VM _vm;
        private int previouschangeSch = 0;
        private int previouschangeSoc = 0;
        public object MessageBoxButtons { get; private set; }


        public MainWindow(CurrentGame x)
        {
            _vm = new MainWindow_VM(x);
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = _vm;
            counter = _vm.Datetime.Year - _vm.PlayerPerson.Birth.Year;
        }

        private void LlblName_Initialized(object sender, EventArgs e)// May need to be removed and replaced with data binding
        {
            lblName.Content = _vm.PlayerPerson.ToString();
        }

        private void BtnEducation_Click(object sender, RoutedEventArgs e)
        {
            if (counter <= 7)
            {
                MessageBoxResult result = MessageBox.Show("You don't go to school yet!", "Not available yet", MessageBoxButton.OK, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        break;
                }
            }
            else if (GrdSchoolChoices.Visibility == Visibility.Visible)
            {
                GrdSchoolChoices.Visibility = Visibility.Hidden;
            }
            else
                GrdSchoolChoices.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e) // testing purposes for random event confirmation
        {
            //MessageBoxResult mssgResult = MessageBox.Show("Sure", "Some Title", MessageBoxButtons.YesNo);
            MessageBoxResult result = MessageBox.Show("Palun tööta ma tahan magama minna!!!", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    MessageBox.Show("You feel as like something has changed", "Random Event");

                    break;
                case MessageBoxResult.No:
                    MessageBox.Show("Nothing interesting happened", "Random Event");
                    break;
            }
        }

        private void BtnSaveGame_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Save Current Game, this wil overwrite existing save", "Save Game", MessageBoxButton.YesNo, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    _vm.SaveCurrentGame();
                    MessageBox.Show("Your game has been saved!");

                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void CboxSocial_DropDownClosed(object sender, EventArgs e)
        {
            _vm.UpdateActionPoints(previouschangeSoc);

            if (cboxSocial.SelectedItem == cboxSocial1)
            {
                _vm.UpdateActionPoints(5);
                previouschangeSoc = -5;
                _vm.RelationToChange = 5;
            }
            else if (cboxSocial.SelectedItem == cboxSocial2)
            {
                _vm.UpdateActionPoints(3);
                previouschangeSoc = -3;
                _vm.RelationToChange = 4;
            }
            else if (cboxSocial.SelectedItem == cboxSocial3)
            {
                previouschangeSoc = 0;
                _vm.RelationToChange = 3;
            }
            else if (cboxSchool.SelectedItem == cboxSchool4)
            {
                _vm.UpdateActionPoints(-2);
                previouschangeSoc = 2;
                _vm.RelationToChange = 2;
            }
        }

        private void CboxSchool_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _vm.UpdateActionPoints(previouschangeSch);

            if (cboxSchool.SelectedItem == cboxSchool1)
            {
                _vm.UpdateActionPoints(5);
                previouschangeSch = -5;
                _vm.IntToChange = 5;
            }
            else if (cboxSchool.SelectedItem == cboxSchool2)
            {
                _vm.UpdateActionPoints(3);
                previouschangeSch = -3;
                _vm.IntToChange = 3;
            }
            else if (cboxSchool.SelectedItem == cboxSchool3)
            {
                previouschangeSch = 0;
                _vm.IntToChange = 2;
            }
            else if (cboxSchool.SelectedItem == cboxSchool4)
            {
                _vm.UpdateActionPoints(-2);
                previouschangeSch = 2;
                _vm.IntToChange = -5;
            }
        }

        private void BtnForwardTime_Click(object sender, RoutedEventArgs e)
        {
            if (_vm.ActionPoints < 0)
            {
                MessageBoxResult result = MessageBox.Show("You don't have enough action points...", "Notification", MessageBoxButton.OK, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        break;
                }
            }
            else
            {
                counter++;
                _vm.ForwardTime();
                

                if (counter == 3)
                {
                    MessageBoxResult result = MessageBox.Show("You have started going to the Kindergarten", "Notification", MessageBoxButton.OK, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.OK:
                            break;
                    }

                }

                else if (counter > 7)
                {
                    MessageBoxResult result = MessageBox.Show("You have started going to School", "Notification", MessageBoxButton.OK, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.OK:
                            break;
                    }
                    EventDisplay(_vm.s);
                }

                //else if (counter == 15)
                //{
                //    MessageBoxResult result = MessageBox.Show("You are offered alcohol do you accept?", "Offer", MessageBoxButton.YesNo, MessageBoxImage.Question);
                //    switch (result)
                //    {

                //        case MessageBoxResult.Yes:
                //            MessageBox.Show("You have now started drinking alcohol and your health will detriorate over time.", "Offer");

                //            break;
                //        case MessageBoxResult.No:
                //            MessageBox.Show("Nothing interesting happened", "Offer");
                //            break;

                //    }
                //}

                else if (counter == 19)
                {
                    MessageBoxResult result = MessageBox.Show("Congratulations you have finished shool!", "Notification", MessageBoxButton.OK, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.OK:
                            break;
                    }
                }

                if (!_vm.PlayerPerson.IsAlive) // Needs to be reworked, here for funct testing
                {
                    GameOverScreen gameOverScreen = new GameOverScreen(_vm._currentGame)
                    {
                        Height = this.Height,
                        Width = this.Width
                    };
                    gameOverScreen.Show();
                    this.Close();
                }
            }
        }

        public void EventDisplay(Events x)
        {

            MessageBoxResult result = MessageBox.Show(x.Info, "Notification", MessageBoxButton.OK, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    break;
            }

        }
        private void lboxRelations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void lboxRelationsRelationLVL_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}