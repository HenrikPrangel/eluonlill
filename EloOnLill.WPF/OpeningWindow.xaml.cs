﻿using EloOnLill.Domain;
using EloOnLill.WPF.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EloOnLill.WPF
{
    /// <summary>
    /// Interaction logic for OpeningWindow.xaml
    /// </summary>
    public partial class OpeningWindow : Window
    {
        private OpeningWindow_VM _vm;
        private CurrentGame _selectedGame;

        public OpeningWindow()
        {
            _vm = new OpeningWindow_VM();
            InitializeComponent();
            this.DataContext = _vm;
            _vm.FirstTimePlayingCheck();

        }

        private void BtnCharacter_click(object sender, RoutedEventArgs e) //Makes the character creation visible
        {
            LboxCurrentGameSelection.Visibility = Visibility.Hidden;
            BtnPlay.Visibility = Visibility.Hidden;
            BtnDelete.Visibility = Visibility.Hidden;

            if (askFirstname.Visibility == Visibility.Hidden)
            {
                TextBlock askForName = new TextBlock
                {
                    Text = "Please insert desired name"
                };
                askName.Children.Add(askForName);

                addChar.Visibility = Visibility.Visible;
                askFirstname.Visibility = Visibility.Visible;
                askLastname.Visibility = Visibility.Visible;
                Chckbx_Male.Visibility = Visibility.Visible;
                Chckbx_FemaleChecked.Visibility = Visibility.Visible;
            }
            else
            {
                askName.Children.Clear();
                addChar.Visibility = Visibility.Hidden;
                askFirstname.Visibility = Visibility.Hidden;
                askLastname.Visibility = Visibility.Hidden;
                Chckbx_Male.Visibility = Visibility.Hidden;
                Chckbx_FemaleChecked.Visibility = Visibility.Hidden;
            }
        }

        private void BtnExisting_Click(object sender, RoutedEventArgs e)
        {
            askName.Children.Clear();
            addChar.Visibility = Visibility.Hidden;
            askFirstname.Visibility = Visibility.Hidden;
            askLastname.Visibility = Visibility.Hidden;
            Chckbx_Male.Visibility = Visibility.Hidden;
            Chckbx_FemaleChecked.Visibility = Visibility.Hidden;

            if (LboxCurrentGameSelection.Visibility == Visibility.Hidden)
            {
                LboxCurrentGameSelection.Visibility = Visibility.Visible;
            }
            else
            {
                LboxCurrentGameSelection.Visibility = Visibility.Hidden;
                BtnPlay.Visibility = Visibility.Hidden;
                BtnDelete.Visibility = Visibility.Hidden;
            }
        }

        private void TxtFocused(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= TxtFocused;
        }

        private void TxtFirstnLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = tb.Text == string.Empty ? "Firstname" : tb.Text;
        }

        private void TxtLastnLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Text = tb.Text == string.Empty ? "Lastname" : tb.Text;
        }

        //Used for creating a new character within the opening window
        private void CreateNewChar(object sender, RoutedEventArgs e)
        {
            //Gets the input info from the view and creates the character
            var newCharacter = new Person
            {
                Firstname = askFirstname.Text,
                Lastname = askLastname.Text,
                Age = 0,
                Attractiveness = 90,
                Health = 100,
                Birth = DateTime.Now,
                Death = DateTime.Now.AddYears(900),
                Happiness = 90,
                Intelligence = 0,
                IsAlive = true
            };
            if (Chckbx_FemaleChecked.IsChecked == true)
            {
                newCharacter.Gender = 2;
            }
            if (Chckbx_Male.IsChecked == true)
            {
                newCharacter.Gender = 1;
            }

            //Check to see, whether all required fields were filled
            if (newCharacter.Firstname.ToLower().Contains("firstname") || newCharacter.Lastname.ToLower().Contains("lastname") || newCharacter.Gender == null)
            {
                askName.Children.Clear();
                TextBlock askForName = new TextBlock
                {
                    Text = "*Please pick a First-, Lastname and gender"
                };
                askName.Children.Add(askForName);
            }
            //If all is in order, a CurrentGame object is created and main window is opened
            else
            {
                CurrentGame CG = _vm.CreateCharacter(newCharacter);
                _selectedGame = CG;
                MainWindow mainWindow = new MainWindow(_selectedGame)
                {
                    Width = this.Width,
                    Height = this.Height
                };
                mainWindow.Show();
                this.Close();
            }
        }

        private void LboxCurrentGameSelection_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(LboxCurrentGameSelection, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                _selectedGame = item.Content as CurrentGame;
                BtnPlay.Visibility = Visibility.Visible;
                BtnDelete.Visibility = Visibility.Visible;
            }
        }

        private void CheckBox_Checked_Female(object sender, RoutedEventArgs e)
        {
            Chckbx_Male.IsChecked = false;
        }

        private void CheckBox_Checked_Male(object sender, RoutedEventArgs e)
        {
            Chckbx_FemaleChecked.IsChecked = false;
        }

        //Used when pressing PLAY button, after having chosen a CurrentGame
        private void PlayGame(object sender, RoutedEventArgs e)
        {
            if (_selectedGame == null)
            {

            }
            MainWindow mainWindow = new MainWindow(_selectedGame)
            {
                Width = this.Width,
                Height = this.Height
            };
            mainWindow.Show();
            this.Close();
        }

        //Used when pressing delete button
        private void RemoveGame(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete: " + _selectedGame, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    _vm.DeleteGame(_selectedGame);
                    MessageBox.Show("Game has been Deleted", "Deleted");
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
    }
}
