﻿using EloOnLill.Domain;
using EloOnLill.Repositories;
using EloOnLill.Repositories.Interfaces;
using EloOnLill.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.WPF.VM
{
    /// <summary>
    /// Main Window ViewModel
    /// Contains all methods, specific to this view
    /// </summary>
    class MainWindow_VM : Base_VM
    {
        private ObservableCollection<PersonRelations> _personRelations;
        private ObservableCollection<int> PersonRelationLevelTemp;
        public CurrentGame _currentGame;
        private string currentStage;
        private CurrentGameService CGS;
        private PersonService PS;
        private PersonRelationsService PRS;
        private RelationService RS;
        public Person PlayerPerson;
        public int _actionPoints;
        public int RelationToChange;
        public int IntToChange;
        private PersonEventsService PERS;
        private EventService ES;
        public Events s;


        public MainWindow_VM(CurrentGame x)
        {
            EloOnLillDbContext2 Db = new EloOnLillDbContext2();
            var ps = new PersonService(Db);
            PS = ps;
            var cgs = new CurrentGameService(Db);
            CGS = cgs;
            var rs = new RelationService(Db);
            RS = rs;
            var prs = new PersonRelationsService(Db);
            PRS = prs;

            var es = new EventService(Db);
            ES = es;
            var pers = new PersonEventsService(Db);
            PERS = pers;


            ActionPoints = 5;
            RelationToChange = 3;
            _personRelations = new ObservableCollection<PersonRelations>();
            EstablishCurrentGame(x);
            

        }

        /// <summary>
        /// List containing RelationLevel values for PersonRelations list.
        /// </summary>
        public ObservableCollection<int> TempRelatLevel
        {
            get { return PersonRelationLevelTemp; }
            private set
            {
                PersonRelationLevelTemp = value;
                base.NotifyPropertyChanged("TempRelatLevel");
            }
        }

        /// <summary>
        /// Int value, which specifies how many actions a player can take, during a turn.
        /// </summary>
        public int ActionPoints
        {
            get { return _actionPoints; }
            private set
            {
                _actionPoints = value;
                base.NotifyPropertyChanged("ActionPoints");
            }
        }

        /// <summary>
        /// List containing all relationships (PersonRelations) for the main character.
        /// </summary>
        public ObservableCollection<PersonRelations> Persons
        {
            get { return _personRelations; }
            private set
            {
                _personRelations = value;
                base.NotifyPropertyChanged("Persons");
            }
        }

        /// <summary>
        /// DateTime value, that needs to be shown in main view
        /// </summary>
        public DateTime Datetime
        {
            get { return _currentGame.GameTime; }
            private set
            {
                _currentGame.GameTime = value;
                base.NotifyPropertyChanged("Datetime");
            }
        }

        /// <summary>
        /// Age of the main character
        /// </summary>
        public int PersonAge
        {
            get { return PlayerPerson.Age; }
            private set
            {
                PlayerPerson.Age = value;
                base.NotifyPropertyChanged("PersonAge");
            }
        }

        /// <summary>
        /// Life stage, that the main character is in
        /// </summary>
        public string PersonStage
        {
            get { return currentStage; }
            private set
            {
                currentStage = value;
                base.NotifyPropertyChanged("PersonStage");
            }
        }

        /// <summary>
        /// Happiness of the main character
        /// </summary>
        public int PersonHappiness

        {
            get { return PlayerPerson.Happiness; }
            private set
            {
                PlayerPerson.Happiness = value;
                base.NotifyPropertyChanged("PersonHappiness");
            }
        }

        /// <summary>
        /// Attractiveness of the main character
        /// </summary>
        public int PersonAttractiveness

        {
            get { return PlayerPerson.Attractiveness; }
            private set
            {
                PlayerPerson.Attractiveness = value;
                base.NotifyPropertyChanged("PersonAttractiveness");
            }
        }

        /// <summary>
        /// Health of the main character
        /// </summary>
        public int PersonHealth

        {
            get { return PlayerPerson.Health; }
            private set
            {
                PlayerPerson.Happiness = value;
                base.NotifyPropertyChanged("PersonHealth");
            }
        }

        /// <summary>
        /// Happiness of the main character
        /// </summary>
        public int PersonIntelligence

        {
            get { return PlayerPerson.Intelligence; }
            private set
            {
                PlayerPerson.Intelligence = value;
                base.NotifyPropertyChanged("PersonIntelligence");
            }
        }

        /// <summary>
        /// Method used to establish the current game, by loading in player stats.
        /// </summary>
        public void EstablishCurrentGame(CurrentGame x)
        {
            LoadCurrentGame(x.CurrentGameID);
            LoadPlayerRelationsData();
            RelationLevelTemp();
        }

        /// <summary>
        /// Method that reloads currentgame from DB to insure no data is lost or corrupted, used only when establishing game.
        /// </summary>
        /// <param name="ID">ID of the CurrentGame object, that needs to be loaded in</param>
        public void LoadCurrentGame(int ID) //Method that loads current game from database
        {
            CurrentGame CG = CGS.FindCurrentGameByID(ID);
            _currentGame = CG;
            PlayerPerson = CG.Person;
            currentStage = _currentGame.OutPutLifeStage();
        }

        /// <summary>
        /// Method that loads the relations of the player, used only when establishing game.
        /// </summary>
        public void LoadPlayerRelationsData()
        {
            var handledPerson = PlayerPerson;
            var x = PRS.GetPersonRelations(handledPerson);
            foreach (var item in x)
            {
                _personRelations.Add(item);
            }
        }

        /// <summary>
        /// Method that loads in relationlevels. Idealy, this method is unneeded, and needs to be replaced.
        /// </summary>
        public void RelationLevelTemp()
        {
            PersonRelationLevelTemp = new ObservableCollection<int>();
            foreach (var item in _personRelations)
            {
                PersonRelationLevelTemp.Add(item.RelationLevel);
            }
        }


        /// <summary>
        /// Method that "saves the game"
        /// </summary>
        /// <param name="cg">Saves the Current Game and all relationships to the DB </param>
        public void SaveCurrentGame(CurrentGame cg) //Method that saves information in current playthrough to the Database
        {
            PS.UpdatePeople(PS.GetPeopleInCurrentgame(cg));
            PRS.UpdatePersonRelationsList(_personRelations);
            CGS.SaveCurrentGame(cg);

        }

        /// <summary>
        /// Method used, for calling out SaveCurrentGame method from other classes
        /// </summary>
        internal void SaveCurrentGame()
        {
            SaveCurrentGame(_currentGame);
        }


        /// <summary>
        /// Method that handles all events and actions, involving the passage of time.
        /// </summary>
        public void ForwardTime()
        {
            //Time is forwarded by 1 year
            _currentGame.GameTime = _currentGame.GameTime.AddYears(1);

            //Player relations and stats are updated
            UpdateStats(IntToChange, RelationToChange);

            //Relationlevels are updated
            RelationLevelTemp();

            //Main character age is updated
            PlayerPerson.Age += 1;

            //Information is updated in VM
            UpdateVM();
            var x = RandomEvent(7,20);
            s = x;
            if (PlayerPerson.Age >= 8)
            {
                UpdateByEvent(x);
            }
          

            //Game End check
            if (PlayerPerson.Age > 20 || PlayerPerson.Health <= 0)
            {
                PlayerPerson.KillOff();
            }
        }

        private void UpdateByEvent(Events x)
        {
            PlayerPerson.Happiness += x.HappinessModifier;
            PlayerPerson.Intelligence += x.InteligenceModifier;
            PlayerPerson.Health += x.HealthModifier;
            PlayerPerson.Attractiveness += x.Atrectivnessmodifier;
        }

        /// <summary>
        /// Method used from other classes, to update actionpoints
        /// </summary>
        internal void UpdateActionPoints(int v)
        {
            _actionPoints -= v;
            ActionPoints = _actionPoints;
        }

        /// <summary>
        /// Method insuring that view information is up to date
        /// </summary>
        public void UpdateVM()
        {
            PersonStage = currentStage;
            Datetime = _currentGame.GameTime;
            PersonHappiness = PlayerPerson.Happiness;
            PersonHealth = PlayerPerson.Health;
            PersonStage = _currentGame.OutPutLifeStage();
            PersonAge = PlayerPerson.Age;
            Persons = _personRelations;
            PersonIntelligence = PlayerPerson.Intelligence;
            PersonAttractiveness = PlayerPerson.Attractiveness;
            TempRelatLevel = PersonRelationLevelTemp;
        }

        /// <summary>
        /// Method that handles relationships, their generation in the game and their updates.
        /// </summary>
        /// <param name="Relation">Value, taken from the view, from the choice box. Determines how much the players relations are going to change, and in which way</param>
        /// <param name="Odd">The odd which by relationship levels are updated</param>
        public void RelationHandle(int Relation, int Odd)
        {
            //Finds a random person from the DB, whose within the given age gap.
            var y = PS.FindRandomPerson(0, 90);
            bool t = false;

            //Handles all the relations that the main character has
            foreach (var item in _personRelations)
            {
                // If the found person is actually the main character, a new character is searched for
                if (y.PersonID == PlayerPerson.PersonID)
                {
                    while (y.PersonID == PlayerPerson.PersonID)
                    {
                        y = PS.FindRandomPerson(0, 90);
                    }
                }
                // All relationships, except familial, have their "level" lowered
                // Currently familial relationships never deteriorate
                if (item.Relation.RelationType != Domain.Enums.RelationType.Family)
                {
                    item.RelationLevel -= 5;
                }
                //If the found person already has a relation with the main character, a huge boost is given to the relationlevel
                if (y.PersonID == item.Person2.PersonID)
                {
                    t = true;
                    item.RelationLevel += 4 * Relation;
                }
                //Updates the relationship level positively, if the odds are in favour
                Random rnd = new Random();
                int rndNumber = rnd.Next(0, 100);
                if (rndNumber >= Odd)
                {
                    item.RelationLevel += 2 * Relation;
                }
                if (item.RelationLevel > 100)
                {
                    item.RelationLevel = 100;
                }
                if (item.RelationLevel < 0)
                {
                    item.RelationLevel = 0;
                }
            }
            //If found person does not have a relationship with player, a new relationship is generated
            if (!t)
            {
                PersonRelations w;
                if (y.Gender == 1)
                {
                    var x = RS.FindRelationByName("Player_Male_Friend");
                    w = new PersonRelations(PlayerPerson, y, x);
                }
                else
                {
                    var x = RS.FindRelationByName("Player_Female_Friend");
                    w = new PersonRelations(PlayerPerson, y, x);
                }
                _personRelations.Add(w);
            }
            Persons = _personRelations;
        }

        /// <summary>
        /// Method that handles the update/progress of main characters stats.
        /// </summary>
        /// <param name="Int">Value, taken from the view, from the choice box. Determines how much the intelligence of the main character is going to change, and in which way</param>
        /// <param name="Relation">Value, taken from the view, from the choice box. Determines how much the players relations are going to change, and in which way</param>
        public void UpdateStats(int Int, int Relation)
        {
            //By standard, the players happiness and health are changed by these amounts
            int Hap = -20;
            int Hel = 5;

            //Updating relationships
            switch (Relation)
            {
                default:
                    RelationHandle(3, 50);
                    break;
                case 5:
                    RelationHandle(5, 10);
                    break;
                case 4:
                    RelationHandle(4, 25);
                    break;
                case 3:
                    RelationHandle(3, 50);
                    break;
                case 2:
                    RelationHandle(2, 75);
                    break;
            }

            //Used for finding the amount of happiness, player is going to get or lose. Happiness is found by relations.
            foreach (var item in _personRelations)
            {
                if (item.RelationLevel >= 90)
                {
                    Hap += 15;
                }
                else if (item.RelationLevel >= 70)
                {
                    Hap += 5;
                }
                else if (item.RelationLevel >= 50)
                {
                    Hap += 3;
                }
                else
                    Hap += 1;
            }

            //Used for finding the amount of health, the player is going to gain or lose.
            switch (_actionPoints)
            {
                case 0:
                    Hel = -10;
                    break;
                case 1:
                    Hel = -5;
                    break;
                case 2:
                    Hel = -1;
                    break;
                case 3:
                    Hel = 0;
                    break;
                case 4:
                    Hel = 3;
                    break;
                case 5:
                    Hel = 5;
                    break;
            }

            //Updating player character stats.
            PlayerPerson.Happiness += Hap;
            PlayerPerson.Health += Hel;
            PlayerPerson.Intelligence += Int;

            //Bug prevention
            if (PlayerPerson.Happiness > 100)
            {
                PlayerPerson.Happiness = 100;
            }
            if (PlayerPerson.Happiness < 0)
            {
                PlayerPerson.Happiness = 0;
            }
            if (PlayerPerson.Health > 100)
            {
                PlayerPerson.Health = 100;
            }
            if (PlayerPerson.Intelligence > 100)
            {
                PlayerPerson.Intelligence = 100;
            }
            if (PlayerPerson.Intelligence < 0)
            {
                PlayerPerson.Intelligence = 0;
            }

        }
        public Events RandomEvent(int AgeStarts, int AgeEnds) {

            Events x;
            //if (PlayerPerson.Age == 4) {
                x = ES.FindRandomEvent(AgeStarts, AgeEnds);

            //}

            return x;
        }
       


    }
}

