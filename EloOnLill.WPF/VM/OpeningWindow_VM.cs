﻿using EloOnLill.Domain;
using EloOnLill.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.WPF.VM
{
    class OpeningWindow_VM : Base_VM
    {
        private List<CurrentGame> _currentGame;
        private CurrentGameService CGS;
        private PersonService PS;
        private RelationService RS;
        private PersonRelationsService PRS;

        /// <summary>
        /// Opening Window ViewModel
        /// Contains all methods, specific to this view
        /// </summary>
        public OpeningWindow_VM()
        {
            EloOnLillDbContext2 Db = new EloOnLillDbContext2();
            var ps = new PersonService(Db);
            PS = ps;
            var cgs = new CurrentGameService(Db);
            CGS = cgs;
            var rs = new RelationService(Db);
            RS = rs;
            var prs = new PersonRelationsService(Db);
            PRS = prs;
            _currentGame = CGS.GetAllCurrentGames();
        }

        /// <summary>
        /// Method checks, whether database is empty or not, and fills it with info if nessecary.
        /// </summary>
        internal void FirstTimePlayingCheck()
        {
            var y = PS.GetPersons().Count();
            if (y < 1)
            {
                //FillDatabaseWithInfo - Code that fills database with info
                var mikk = new Person
                {
                    Firstname = "Mart",
                    Lastname = "Pooru",
                    Age = 0,
                    Gender = 1,
                    Attractiveness = 90,
                    Health = 100,
                    Birth = DateTime.Now,
                    Death = DateTime.Now.AddYears(900),
                    Happiness = 90,
                    Intelligence = 0,
                    IsAlive = true
                };
                Relations Player_Mother = new Relations("Player_Mother", Domain.Enums.RelationType.Family, "p2");
                Relations Player_Father = new Relations("Player_Father", Domain.Enums.RelationType.Family, "p1");
                Relations Mother_Father = new Relations("Mother_Father", Domain.Enums.RelationType.Family, "21");
                Relations Player_Female_Friend = new Relations("Player_Female_Friend", Domain.Enums.RelationType.Friend, "p2");
                Relations Player_Male_Friend = new Relations("Player_Male_Friend", Domain.Enums.RelationType.Friend, "p1");
                RS.AddRelations(Player_Father);
                RS.AddRelations(Player_Mother);
                RS.AddRelations(Mother_Father);
                RS.AddRelations(Player_Female_Friend);
                RS.AddRelations(Player_Male_Friend);
                PS.AddPerson(mikk);
                PS.AddPersonManual("Lille", "Laanemets", 2, 30);
                PS.AddPersonManual("Tommi", "Evangelico", 1, 31);
                PS.AddPersonManual("Aina", "Remmel", 2, 34);
                var x = PS.FindByName("Mart", "Pooru");
                var z = PS.FindByName("Lille", "Laanemets");
                var w = PS.FindByName("Tommi", "Evangelico");
                PRS.Create2ParentFamilyRelation(x, w, z);
                var relation = RS.FindRelationByName(" Player_Father");
                var persons = PS.GetPersons();
                var PersRelat = new PersonRelations(persons[1], persons[2], relation);
            }
        }

        /// <summary>
        /// List of CurrentGame objects
        /// </summary>
        public List<CurrentGame> CurrentGames
        {
            get { return _currentGame; }
            private set
            {
                _currentGame = value;
                base.NotifyPropertyChanged("CurrentGames");
            }
        }

        /// <summary>
        /// Method used for creating a new main character from within the game
        /// </summary>
        internal CurrentGame CreateCharacter(Person person)
        {
            PS.AddPerson(person);
            var z = PS.FindByName("Lille", "Laanemets");
            var w = PS.FindByName("Tommi", "Evangelico");
            PRS.Create2ParentFamilyRelation(person, w, z);
            var CurrentGame = new CurrentGame(person)
            {
                GameTime = DateTime.Today
            };
            CGS.AddCurrentGame(CurrentGame);
            return CurrentGame;
        }

        /// <summary>
        /// Method used for deleting CurrentGame objects (Save Games)
        /// </summary>
        internal void DeleteGame(CurrentGame CG)
        {
            var toDelete = PRS.GetPersonRelations(CG.Person);
            PRS.RemoveRelations(toDelete);
            CGS.RemoveCurrentGame(CG);
            CurrentGames = CGS.GetAllCurrentGames();
        }
    }
}
