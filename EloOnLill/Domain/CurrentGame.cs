﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain
{
    /// <summary>
    /// CurrentGame class.
    /// CurrentGame essentially acts as a "save game", containing information about the player and game playing time
    /// Also contains all methods for performing operations with CurrentGame.
    /// </summary>
    /// <remarks>
    /// This class can output the "life stage" and person along with gametime.
    /// </remarks>
    public class CurrentGame
    {
        public int CurrentGameID { get; set; }

        /// <summary>
        /// Datetime of the current game. Mostly gets set automatically.
        /// </summary>
        [Required]
        public DateTime GameTime { get; set; }

        public int PersonID { get; set; }
        public virtual Person Person { get; set; }

        public CurrentGame()
        {
        }
        public CurrentGame(Person person)
        {
            Person = person;
            GameTime = DateTime.Now;
        }

        public override string ToString()
        {
            return $"Player: {Person}, Gametime: {GameTime}";
        }

        /// <summary>
        /// Outputs the current lifestage of the character, stored within CurrentGame
        /// </summary>
        /// <returns>
        /// The name of the life stage in string.
        /// </returns>
        public string OutPutLifeStage()
        {
            if (Person.Age <= 3)
            {
                return "Baby";
            }
            else if (Person.Age <= 13)
            {
                return "Child";
            }
            else if (Person.Age <= 18)
            {
                return "Teenager";
            }
            else return "Young Adult";
        }
    }
}
