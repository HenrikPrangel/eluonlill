﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain.Enums
{
    public enum RelationType
    {
        Family = 1,
        Friend = 2,
        Lover = 3,
    };
}
