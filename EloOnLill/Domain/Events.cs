﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain
{
    public class Events
    {
        public int EventsID { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public int AgeStarts { get; set; }
        public int AgeEnds { get; set; }
        public int HappinessModifier { get; set; }
        public int InteligenceModifier { get; set; }
        public int Atrectivnessmodifier { get; set; }
        public int HealthModifier { get; set; }
        public virtual List<PersonEvents> Vambola { get; set; }


        public  Events() {


        }
        public static void StatChanger(Person person, Events Eve)
        {

            person.Attractiveness += Eve.Atrectivnessmodifier;
            person.Health += Eve.HealthModifier;
            person.Intelligence += Eve.InteligenceModifier;
            person.Happiness += Eve.HappinessModifier;

          
        }
    }
}

       




