﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain
{
    /// <summary>
    /// Person class.
    /// Holds all information for persons(characters) in the game, aswell as
    /// methods for dealing with persons.
    /// </summary>
    /// <remarks>
    /// This class can output the "name" of the person aswell as declare the person as "dead".
    /// </remarks>
    public class Person
    {
        public int PersonID { get; set; }


        [Required]
        [MaxLength(length: 50)]
        public string Firstname { get; set; }

        [Required]
        [MaxLength(length: 50)]
        public string Lastname { get; set; }

        [Required]
        public int Age { get; set; }

        /// <summary>
        /// The Gender of a person: 1 for male, 2 for female
        /// </summary>
        [Required]
        public byte Gender { get; set; }

        /// <summary>
        /// Boolean value to define, whether person is dead or not
        /// </summary>
        [Required]
        public bool IsAlive { get; set; }

        /// <summary>
        /// Date of birth.
        /// </summary>
        [Required]
        public DateTime Birth { get; set; }

        /// <summary>
        /// Date of death.
        /// </summary>
        public DateTime Death { get; set; }

        /// <summary>
        /// Value that specifies how happy the character is, 0-sad, 100-The happiest
        /// </summary>
        public int Happiness { get; set; }
        /// <summary>
        /// Value that specifies how attractive the character is, 0-Hideous, 100-Gorgeous
        /// </summary>
        public int Attractiveness { get; set; }
        /// <summary>
        /// Value that specifies how intelligent the character is, 0-Idiot, 100-Genius
        /// </summary>
        public int Intelligence { get; set; }
        /// <summary>
        /// Value that specifies how courageous the character is in socialising with other people. 0-shy, 100-courageous
        /// </summary>
        public int Social { get; set; }
        /// <summary>
        /// Value that specifies how much wealth the person has accumulated
        /// </summary>
        public double Money { get; set; }
        /// <summary>
        /// Value that specifies how healthy the character is. 0-Dying, 100-Healthy
        /// </summary>
        public int Health { get; set; }

        public virtual List<Relations> PersonRelations { get; set; }
        public virtual List<CurrentGame> CurrentGames { get; set; }
        public virtual List<PersonEvents> vambola { get; set; }
        public Person()
        {
        }

        public Person(String firstname, String lastname, int age, int attractiveness, byte gender, int happiness, int health)
        {
            Firstname = firstname;
            Lastname = lastname;
            Age = age;
            Gender = gender;
            Attractiveness = attractiveness;
            IsAlive = true;
            Health = health;
            Happiness = happiness;
        }

        /// <summary>
        /// Declares the person as "dead" and assigns the death date of the person
        /// </summary>
        public void KillOff()
        {
            this.IsAlive = false;
            this.Death = DateTime.Now;
        }

        public override string ToString()
        {
            return $"{Firstname} {Lastname}";
        }
    }
}
