﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain
{
    public class PersonEvents
    {

        public int PersonEventsID { get; set; }
        public int Person_PersonID { get; set; }
        public virtual Person Person { get; set; }
        public int Event_Events_ID { get; set; }
        public virtual Events Event{ get; set; }
        public override string ToString()
        {
            return $"{Person}: {Event.Name}";
        }

    }
}


//        public void BigLifeEvents(Person person)
//        {

//            if (person.Age == 3)
//            {
//                Events.Kindergarten(person);
//            }

//            else if (person.Age == 7)
//            {
//                Events.School(person);

//            }
//            else
//            {

//            }
//        }
//        public static void RandomEvents(Person person)
//        {

//            Events.Meteorite(person);
//            Events.Drugs(person);


//        }


//    }
//}
