﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain
{
    /// <summary>
    /// PersonRelations class.
    /// Holds all information for persons(characters) in relations with other persons.
    /// Contains methods specific to PersonRelations class.
    /// </summary>
    /// <remarks>
    /// Contains only 1 method, that declares the relationship as "over".
    /// </remarks>
    public class PersonRelations 
    {
        public int PersonRelationsID { get; set; }
        public int Person1_PersonID { get; set; }
        public virtual Person Person1 { get; set; }

        public int Person2_PersonID { get; set; }
        public virtual Person Person2 { get; set; }

        [Required]
        public int Relation_RelationsID { get; set; }
        public virtual Relations Relation { get; set; }

        /// <summary>
        /// Value that shows the "quality" of the relationship: 0-bad, 100-good
        /// </summary>
        [Required]
        public int RelationLevel { get; set; }

        /// <summary>
        /// Date when this relationship started
        /// </summary>
        [Required]
        public DateTime From { get; set; }

        /// <summary>
        /// Date when this relationship ended
        /// </summary>
        [Required]
        public DateTime Until { get; set; }


        public PersonRelations()
        {

        }

        public PersonRelations(Person Player, Person person, Relations relations)
        {
            Person1= Player;
            Person2 = person;
            RelationLevel = 50;
            Relation = relations;
            From = DateTime.Now;
            Until = DateTime.Now.AddYears(900);
        }

        /// <summary>
        /// Declares the relationship "over" by assigning an end date to the relationship
        /// </summary>
        public void RelationOver()
        {
            this.Until = DateTime.Now;
        }

        public override string ToString()
        {
            return $"{this.Person2}: {Relation.RelationType}";
        }
    }
}
    