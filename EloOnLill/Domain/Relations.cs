﻿using EloOnLill.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Domain
{
    /// <summary>
    /// Relations class.
    /// Holds all information for relations, that persons can be in.
    /// </summary>
    public class Relations
    {
        public int RelationsID { get; set; }

        /// <summary>
        /// Name of the relation
        /// </summary>
        [Required]
        [MaxLength(length: 500)]
        public String RelationName { get; set; }

        /// <summary>
        /// Specifies the type of the relation
        /// </summary>
        /// <example>
        /// Familial, friendly, love related...
        /// </example>
        [Required]
        public RelationType RelationType { get; set; }

        /// <summary>
        /// Specifies between whom this relation can exist. P=player, 1= Man, 2= Woman
        /// </summary>
        /// <example>
        /// P1 or  P2 or 12
        /// </example>
        [Required]
        [MaxLength(length: 10)]
        public string RelationCode { get; set; }

        public virtual List<PersonRelations> PersonRelations { get; set; }

        public Relations()
        {
        }

        public Relations(String relationName, RelationType relationType, string relationCode)
        {
            RelationName = relationName;
            RelationType = relationType;
            RelationCode = relationCode;
        }

        public override string ToString()
        {
            return $"Relation: {this.RelationName}";
        }
    }
}
