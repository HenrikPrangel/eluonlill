﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill
{
    public class EloOnLillDbContext:DbContext, IEluOnLillContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<PersonRelations> PersonRelations { get; set; }
        public DbSet<Relations> Relations { get; set; }

        public EloOnLillDbContext() : base("name=EloOnLillSql")
        {
        }
    }
}
