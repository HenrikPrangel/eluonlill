﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill
{
    public class EloOnLillDbContext2:DbContext, IEluOnLillContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<PersonRelations> PersonRelations { get; set; }
        public DbSet<Relations> Relations { get; set; }
        public DbSet<CurrentGame> CurrentGame { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<PersonEvents> PersonEvents { get; set; }
        public EloOnLillDbContext2() : base("name=EloOnLillSql2")
        {
        }
    }
}
