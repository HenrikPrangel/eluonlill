namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HopefullyFinalFix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PersonRelations", "Relation_RelationsID", "dbo.Relations");
            DropIndex("dbo.PersonRelations", new[] { "Relation_RelationsID" });
            AddColumn("dbo.PersonRelations", "Relation_RelationsID1", c => c.Int());
            CreateIndex("dbo.PersonRelations", "Relation_RelationsID1");
            AddForeignKey("dbo.PersonRelations", "Relation_RelationsID1", "dbo.Relations", "RelationsID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonRelations", "Relation_RelationsID1", "dbo.Relations");
            DropIndex("dbo.PersonRelations", new[] { "Relation_RelationsID1" });
            DropColumn("dbo.PersonRelations", "Relation_RelationsID1");
            CreateIndex("dbo.PersonRelations", "Relation_RelationsID");
            AddForeignKey("dbo.PersonRelations", "Relation_RelationsID", "dbo.Relations", "RelationsID");
        }
    }
}
