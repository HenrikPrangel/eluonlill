namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.People",
                c => new
                    {
                        PersonID = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false, maxLength: 50),
                        Lastname = c.String(nullable: false, maxLength: 50),
                        Age = c.Int(nullable: false),
                        Gender = c.Byte(nullable: false),
                        IsAlive = c.Boolean(nullable: false),
                        Happiness = c.Int(nullable: false),
                        Attractiveness = c.Int(nullable: false),
                        Birth = c.DateTime(nullable: false),
                        Death = c.DateTime(nullable: false),
                        Intelligence = c.Int(nullable: false),
                        Social = c.Int(nullable: false),
                        Money = c.Double(nullable: false),
                        Health = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PersonID);
            
            CreateTable(
                "dbo.Relations",
                c => new
                    {
                        RelationsID = c.Int(nullable: false, identity: true),
                        RelationName = c.String(nullable: false, maxLength: 500),
                        RelationType = c.Int(nullable: false),
                        Person_PersonID = c.Int(),
                    })
                .PrimaryKey(t => t.RelationsID)
                .ForeignKey("dbo.People", t => t.Person_PersonID)
                .Index(t => t.Person_PersonID);
            
            CreateTable(
                "dbo.PersonRelations",
                c => new
                    {
                        PersonRelationsID = c.Int(nullable: false, identity: true),
                        Person1_PersonID = c.Int(nullable: false),
                        Person2_PersonID = c.Int(nullable: false),
                        Relation_RelationsID = c.Int(nullable: false),
                        RelationLevel = c.Int(nullable: false),
                        From = c.DateTime(nullable: false),
                        Until = c.DateTime(nullable: false),
                        Person1_PersonID1 = c.Int(),
                        Person2_PersonID1 = c.Int(),
                        Relation_RelationsID1 = c.Int(),
                    })
                .PrimaryKey(t => t.PersonRelationsID)
                .ForeignKey("dbo.People", t => t.Person1_PersonID1)
                .ForeignKey("dbo.People", t => t.Person2_PersonID1)
                .ForeignKey("dbo.Relations", t => t.Relation_RelationsID1)
                .Index(t => t.Person1_PersonID1)
                .Index(t => t.Person2_PersonID1)
                .Index(t => t.Relation_RelationsID1);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Relations", "Person_PersonID", "dbo.People");
            DropForeignKey("dbo.PersonRelations", "Relation_RelationsID1", "dbo.Relations");
            DropForeignKey("dbo.PersonRelations", "Person2_PersonID1", "dbo.People");
            DropForeignKey("dbo.PersonRelations", "Person1_PersonID1", "dbo.People");
            DropIndex("dbo.PersonRelations", new[] { "Relation_RelationsID1" });
            DropIndex("dbo.PersonRelations", new[] { "Person2_PersonID1" });
            DropIndex("dbo.PersonRelations", new[] { "Person1_PersonID1" });
            DropIndex("dbo.Relations", new[] { "Person_PersonID" });
            DropTable("dbo.PersonRelations");
            DropTable("dbo.Relations");
            DropTable("dbo.People");
        }
    }
}
