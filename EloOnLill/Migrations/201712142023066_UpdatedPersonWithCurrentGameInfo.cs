namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedPersonWithCurrentGameInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CurrentGames",
                c => new
                    {
                        CurrentGameID = c.Int(nullable: false, identity: true),
                        GameTime = c.DateTime(nullable: false),
                        PersonID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CurrentGameID)
                .ForeignKey("dbo.People", t => t.PersonID, cascadeDelete: true)
                .Index(t => t.PersonID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CurrentGames", "PersonID", "dbo.People");
            DropIndex("dbo.CurrentGames", new[] { "PersonID" });
            DropTable("dbo.CurrentGames");
        }
    }
}
