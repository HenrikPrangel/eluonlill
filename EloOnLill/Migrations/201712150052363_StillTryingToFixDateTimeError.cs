namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StillTryingToFixDateTimeError : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "Birth", c => c.DateTime(nullable: false));
            AlterColumn("dbo.People", "Death", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "Death", c => c.DateTime(nullable: false));
            AlterColumn("dbo.People", "Birth", c => c.DateTime(nullable: false));
        }
    }
}
