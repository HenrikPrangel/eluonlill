namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatingCurrentGame : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CurrentGames", "GameTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CurrentGames", "GameTime", c => c.DateTime(nullable: false));
        }
    }
}
