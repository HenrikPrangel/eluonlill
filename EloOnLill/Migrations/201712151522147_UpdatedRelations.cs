namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedRelations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Relations", "RelationCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Relations", "RelationCode");
        }
    }
}
