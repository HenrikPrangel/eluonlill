namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAnnotationsUpdate_In_Persons_PersonRelations_Relations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Relations", "RelationCode", c => c.String(nullable: false, maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Relations", "RelationCode", c => c.String());
        }
    }
}
