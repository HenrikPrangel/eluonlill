namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventsMigr1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PersonEvents",
                c => new
                    {
                        PersonEventsID = c.Int(nullable: false, identity: true),
                        Person_PersonID = c.Int(nullable: false),
                        EventID = c.Int(nullable: false),
                        Person_PersonID1 = c.Int(),
                        Relation_RelationsID = c.Int(),
                    })
                .PrimaryKey(t => t.PersonEventsID)
                .ForeignKey("dbo.People", t => t.Person_PersonID1)
                .ForeignKey("dbo.Relations", t => t.Relation_RelationsID)
                .Index(t => t.Person_PersonID1)
                .Index(t => t.Relation_RelationsID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonEvents", "Relation_RelationsID", "dbo.Relations");
            DropForeignKey("dbo.PersonEvents", "Person_PersonID1", "dbo.People");
            DropIndex("dbo.PersonEvents", new[] { "Relation_RelationsID" });
            DropIndex("dbo.PersonEvents", new[] { "Person_PersonID1" });
            DropTable("dbo.PersonEvents");
        }
    }
}
