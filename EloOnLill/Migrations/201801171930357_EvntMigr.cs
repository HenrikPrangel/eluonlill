namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EvntMigr : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PersonEvents", "Action_ID", c => c.Int(nullable: false));
            DropColumn("dbo.PersonEvents", "EventID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PersonEvents", "EventID", c => c.Int(nullable: false));
            DropColumn("dbo.PersonEvents", "Action_ID");
        }
    }
}
