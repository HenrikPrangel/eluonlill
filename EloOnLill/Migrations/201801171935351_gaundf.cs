namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class gaundf : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PersonEvents", "Relation_RelationsID", "dbo.Relations");
            DropIndex("dbo.PersonEvents", new[] { "Relation_RelationsID" });
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventsID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Info = c.String(),
                        AgeStarts = c.Int(nullable: false),
                        AgeEnds = c.Int(nullable: false),
                        HappinessModifier = c.Int(nullable: false),
                        InteligenceModifier = c.Int(nullable: false),
                        Atrectivnessmodifier = c.Int(nullable: false),
                        HealthModifier = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventsID);
            
            AddColumn("dbo.PersonEvents", "Event_Events_ID", c => c.Int(nullable: false));
            AddColumn("dbo.PersonEvents", "Event_EventsID", c => c.Int());
            CreateIndex("dbo.PersonEvents", "Event_EventsID");
            AddForeignKey("dbo.PersonEvents", "Event_EventsID", "dbo.Events", "EventsID");
            DropColumn("dbo.PersonEvents", "Action_ID");
            DropColumn("dbo.PersonEvents", "Relation_RelationsID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PersonEvents", "Relation_RelationsID", c => c.Int());
            AddColumn("dbo.PersonEvents", "Action_ID", c => c.Int(nullable: false));
            DropForeignKey("dbo.PersonEvents", "Event_EventsID", "dbo.Events");
            DropIndex("dbo.PersonEvents", new[] { "Event_EventsID" });
            DropColumn("dbo.PersonEvents", "Event_EventsID");
            DropColumn("dbo.PersonEvents", "Event_Events_ID");
            DropTable("dbo.Events");
            CreateIndex("dbo.PersonEvents", "Relation_RelationsID");
            AddForeignKey("dbo.PersonEvents", "Relation_RelationsID", "dbo.Relations", "RelationsID");
        }
    }
}
