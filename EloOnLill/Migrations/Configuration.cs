namespace EloOnLill.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EloOnLill.EloOnLillDbContext2>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "EloOnLill.EloOnLillDbContext2";
        }

        protected override void Seed(EloOnLill.EloOnLillDbContext2 context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
