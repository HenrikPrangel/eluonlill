﻿using EloOnLill.Domain;
using EloOnLill.Repositories;
using EloOnLill.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EloOnLill.Services;

namespace EloOnLill
{
    class Program
    {
        static void Main(string[] args)
        {
            EloOnLillDbContext2 Db = new EloOnLillDbContext2();
            var PSS = new PersonService(Db);
            //var CGS = new CurrentGameService(Db);
            var RS = new RelationService(Db);
            var ES = new EventService(Db);
            var PS = new PersonEventsService(Db);

            //var PRS = new PersonRelationsService(Db);


            var r = new Events
            {

                Name = "RHSmoke",
                Info = "Your character tryed smoking",
                AgeStarts = 10,
                AgeEnds = 20,
                HappinessModifier = +5,
                InteligenceModifier = 0,
                Atrectivnessmodifier = 0,
                HealthModifier = -15,
            };

            var g = new Events
            {

                Name = "RHDrink",
                Info = "Your character tryed drinking",
                AgeStarts = 13,
                AgeEnds = 20,
                HappinessModifier = +10,
                InteligenceModifier = -10,
                Atrectivnessmodifier = -7,
                HealthModifier = -10,
            };

            var s = new Events
            {

                Name = "RHDroge",
                Info = "Your character tryed Drugs",
                AgeStarts = 13,
                AgeEnds = 20,
                HappinessModifier = +15,
                InteligenceModifier = 0,
                Atrectivnessmodifier = -17,
                HealthModifier = -10,
            };

            var a = new Events
            {

                Name = "Broken dinosaur",
                Info = "Your bring your favourite dinosaurus to kindergarten and it gets broken",
                AgeStarts = 3,
                AgeEnds = 5,
                HappinessModifier = -25,
                InteligenceModifier = 0,
                Atrectivnessmodifier = 0,
                HealthModifier = 0,
            };

            var b = new Events
            {

                Name = "CandyChocke",
                Info = "Your chocke on a piece of candy",
                AgeStarts = 3,
                AgeEnds = 20,
                HappinessModifier = -15,
                InteligenceModifier = 0,
                Atrectivnessmodifier = 0,
                HealthModifier = -25,

            };

            var e = new Events
            {

                Name = "NewBook",
                Info = "Your aquire a new book",
                AgeStarts = 7,
                AgeEnds = 20,
                HappinessModifier = +7,
                InteligenceModifier = +10,
                Atrectivnessmodifier = 0,
                HealthModifier = +1,

            };

            var d = new Events
            {

                Name = "GoodWorkout",
                Info = "Your have a good workout",
                AgeStarts = 16,
                AgeEnds = 20,
                HappinessModifier = +10,
                InteligenceModifier = 0,
                Atrectivnessmodifier = +10,
                HealthModifier = +15,

            };

            var f = new Events
            {

                Name = "BadWorkout",
                Info = "Your injure your self whilst workingout",
                AgeStarts = 16,
                AgeEnds = 20,
                HappinessModifier = -15,
                InteligenceModifier = 0,
                Atrectivnessmodifier = 0,
                HealthModifier = -30,

            };

            var j = new Events
            {

                Name = "ManInAVan",
                Info = "You follow a sketchy looking man into a white van",
                AgeStarts = 6,
                AgeEnds = 15,
                HappinessModifier = -25,
                InteligenceModifier = 0,
                Atrectivnessmodifier = 0,
                HealthModifier = -25,

            };

            var h = new Events
            {

                Name = "StuntVideo",
                Info = "Your try to film a stuntvideo",
                AgeStarts = 11,
                AgeEnds = 16,
                HappinessModifier = -15,
                InteligenceModifier = -30,
                Atrectivnessmodifier = 0,
                HealthModifier = -25,

            };
            var i = new Events
            {

                Name = "Money",
                Info = "You find 5€ on the ground",
                AgeStarts = 3,
                AgeEnds = 20,
                HappinessModifier = +15,
                InteligenceModifier = 0,
                Atrectivnessmodifier = 0,
                HealthModifier = 0,

            };



            var y = PSS.FindByID(17);
            var z = ES.FindByID(2);

            var c = new PersonEvents
            {
                Person = y,
                Event = z
            };

            //PS.AddToDatabase(c);

            //var l = new PersonEvents
            //{
            //    PersonEventsID = 2,
            //    Person_PersonID = 2,
            //    Event_Events_ID = 2,
            //};

            //ES.AddToDatabase(a);
            //ES.AddToDatabase(b);
            //ES.AddToDatabase(d);
            //ES.AddToDatabase(e);
            //ES.AddToDatabase(f);
            //ES.AddToDatabase(g);
            //ES.AddToDatabase(h);
            //ES.AddToDatabase(i);
            //ES.AddToDatabase(j);
            //ES.AddToDatabase(s);


            //PS.AddToDatabase(c);
            //PS.AddToDatabase(l);


            //    var x = PS.GetFromDatabase(y);
            //    foreach (var asi in x)
            //        //{
            //        Console.WriteLine(asi);
            //}



            //Relations Player_Female_Friend = new Relations("Player_Female_Friend", Domain.Enums.RelationType.Friend, "p2");
            //Relations Player_Male_Friend = new Relations("Player_Male_Friend", Domain.Enums.RelationType.Friend, "p1");
            //RS.AddRelations(Player_Female_Friend);
            //RS.AddRelations(Player_Male_Friend);


            //var z = PS.FindByName("Eno", "Lillemets");
            //var w = PS.FindByName("Rando", "Tommis");

            //var relation = RS.FindRelationByName("Mother_Father");
            //var PersRelat = new PersonRelations(z, w, relation);
            //PRS.AddPersonRelations(PersRelat);


            ////////FillDatabaseWithInfo - Code that fills database with info
            //var mikk = new Person
            //{
            //    Firstname = "Mikk",
            //    Lastname = "Mongo",
            //    Age = 0,
            //    Gender = 1,
            //    Attractiveness = 90,
            //    Health = 100,
            //    Birth = DateTime.Now,
            //    Death = DateTime.Now.AddYears(900),
            //    Happiness = 90,
            //    Intelligence = 0,
            //    IsAlive = true
            //};
            //Relations Player_Mother = new Relations("Player_Mother", Domain.Enums.RelationType.Family, "p2");
            //Relations Player_Father = new Relations("Player_Father", Domain.Enums.RelationType.Family, "p1");
            //Relations Mother_Father = new Relations("Mother_Father", Domain.Enums.RelationType.Family, "21");
            //RS.AddRelations(Player_Father);
            //RS.AddRelations(Player_Mother);
            //RS.AddRelations(Mother_Father);
            //PS.AddPerson(mikk);
            //PS.AddPersonManual("Eno", "Lillemets", 2, 89);
            //PS.AddPersonManual("Rando", "Tommis", 1, 10);
            //PS.AddPersonManual("Eva", "Raidla", 2, 34);
            //var x = PS.FindByName("Mikk", "Mongo");
            //var z = PS.FindByName("Eno", "Lillemets");
            //var w = PS.FindByName("Rando", "Tommis");
            //PRS.Create2ParentFamilyRelation(x, w, z);
            //var relation = RS.FindRelationByName(" Player_Father");
            //var persons = PS.GetPersons();
            //var PersRelat = new PersonRelations(persons[1], persons[2], relation); // Relation is null probably, need to fix at some point
            //var CurrentGame = new CurrentGame(x);
            //CGS.AddCurrentGame(CurrentGame);



        }
    }
}
