﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Repositories.Interfaces
{
    public interface IRelationsRepository : IEFRepository<Relations>
    {
    }
}
