﻿using EloOnLill.Domain;
using EloOnLill.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Repositories
{
    public class PersonRelationsRepository : EFRepository<PersonRelations>, IPersonRelationsRepository
    {
        public PersonRelationsRepository(IEluOnLillContext dbContext) : base(dbContext)
        {
        }
    }
}
