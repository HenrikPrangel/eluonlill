﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services
{
    /// <summary>
    /// Base service class
    /// </summary>
    public class BaseService
    {
        protected EloOnLillDbContext2 Db;

        public BaseService(EloOnLillDbContext2 ctx)
        {
            Db = ctx;
        }
    }
}
