﻿using EloOnLill.Domain;
using EloOnLill.Repositories;
using EloOnLill.Repositories.Interfaces;
using EloOnLill.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services
{

    /// <summary>
    /// CurrentGame Service.
    /// Contains services relating to CurrentGame class.
    /// </summary>
    /// <remarks>
    /// Can add, find, remove, save and get all CurrentGames as a list.
    /// </remarks>
    public class CurrentGameService : BaseService, ICurrentGameService
    {
        public CurrentGameService(EloOnLillDbContext2 dbContext2) : base(dbContext2)
        {

        }

        /// <summary>
        /// Adds the input CurrentGame to the DB. Also checks whether handled CurrentGame already exists in the DB in which case it updates handled CurrentGame.
        /// </summary>
        /// <param name="CG"> CurrentGame that needs to be added to the DB</param>
        public void AddCurrentGame(CurrentGame CG)
        {
            var dbCheck = Db.CurrentGame.Where(x => x.CurrentGameID == CG.CurrentGameID).FirstOrDefault();
            var isNew = dbCheck == null;
            if (isNew)
            {
                CG.GameTime = DateTime.Now;
                Db.CurrentGame.Add(CG);
            }
            else
            {
                dbCheck = CG;
            }
            Db.SaveChanges();
        }

        /// <summary>
        /// Finds a CurrentGame from the DB by its ID
        /// </summary>
        /// <param name="ID"> ID of the CurrentGame we want to find</param>
        public CurrentGame FindCurrentGameByID(int ID)
        {
            CurrentGame CG = Db.CurrentGame.Find(ID);
            return CG;
        }

        /// <summary>
        /// Returns all CurrentGames in the DB
        /// </summary>
        /// <returns>
        /// list of CurrentGames
        /// </returns>
        public List<CurrentGame> GetAllCurrentGames()
        {
            return Db.CurrentGame.ToList();
        }

        /// <summary>
        /// Removes the input CG from the DB
        /// </summary>
        /// <param name="CG"> the CurrentGame object, we want to remove from the DB</param>
        public void RemoveCurrentGame(CurrentGame CG)
        {
            Db.CurrentGame.Remove(CG);
            Db.SaveChanges();
        }

        /// <summary>
        /// Saves the input CurrentGame into the DB
        /// </summary>
        /// <param name="CG"> the CurrentGame object, we want to save to the DB</param>
        public void SaveCurrentGame(CurrentGame CG)
        {
            var dbCurrentGame = Db.CurrentGame.Where(p => p.CurrentGameID == CG.CurrentGameID).FirstOrDefault();
            dbCurrentGame = CG;
            Db.SaveChanges();
        }
    }
}
