﻿using EloOnLill.Domain;
using EloOnLill.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services
{
   public class EventService : BaseService, IEventService
    {
        public EventService(EloOnLillDbContext2 ctx) : base(ctx) { }

        public void AddToDatabase(Events r)
        {
            var dbCheck = Db.Events.Where(p => p.EventsID == r.EventsID || p.Name == r.Name).FirstOrDefault();
            var isNew = dbCheck == null;
            if (isNew)
            {
               
                Db.Events.Add(r);
            }
            else
            {
                dbCheck = r;
            }
            Db.SaveChanges();
        }

       
        //evendi id otsing
        public Events FindByID(int ID)
        {
            return Db.Events.Where(x => x.EventsID == ID).Single();
        }

        public int GetFromDatabase()
        {
            throw new NotImplementedException();
        }
        public Events FindRandomEvent(int AgeStarts, int AgeEnds)
        {
            var y = Db.Events.Where(x => x.AgeEnds >= AgeStarts && x.AgeStarts <= AgeEnds).ToList();
            Random rnd = new Random();
            return y[rnd.Next(0, y.Count() - 1)];
        }




        //public Events FindRandomEvent(int AgeStarts, int AgeEnds) 
        //{
        //    var y = Db.Events.Where(x => x.AgeStarts == AgeStarts && x.AgeEnds == AgeEnds).ToList();
        //    Random rnd = new Random();
        //    return y[rnd.Next(0, y.Count() - 1)];
        //}
    }
}
