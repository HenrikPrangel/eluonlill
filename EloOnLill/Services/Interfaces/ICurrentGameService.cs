﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services.Interfaces
{
    public interface ICurrentGameService
    {
        void AddCurrentGame(CurrentGame CG);

        void SaveCurrentGame(CurrentGame CG);

        void RemoveCurrentGame(CurrentGame CG);

        CurrentGame FindCurrentGameByID(int ID);

        List<CurrentGame> GetAllCurrentGames();
    }
}
