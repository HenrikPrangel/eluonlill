﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services.Interfaces
{
    public interface IEventService
    {
         void AddToDatabase(Events r);
        int GetFromDatabase();
    }
}
