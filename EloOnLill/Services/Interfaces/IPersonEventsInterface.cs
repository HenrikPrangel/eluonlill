﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services.Interfaces
{
    interface IPersonEventsInterface
    {
        void AddToDatabase(PersonEvents v);
        void RemoveFromDatabase(PersonEvents v);
        ObservableCollection<PersonEvents> GetFromDatabase(Person p);
    }
}
