﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services.Interfaces
{
    public interface IPersonRelationsService
    {
        void AddPersonRelations(PersonRelations pr);

        void AddPersonRelationsManual(Person Player, Person person, Relations relations, int relationLevel);

        List<PersonRelations> GetPersonRelations(Person person);

        void Create2ParentFamilyRelation(Person player, Person father, Person mother);

        void RemoveRelations(List<PersonRelations> prL);
    }
}
