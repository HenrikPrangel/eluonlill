﻿using EloOnLill.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services.Interfaces
{
    public interface IPersonService
    {
        Person FindByID(int ID);

        Person FindByName(string firstname, string lastname);

        void AddPerson(Person person);

        void RemovePerson(Person person);

        void AddPersonManual(string firstname, string lastname, byte gender, int age);

        Person FindRandomPerson(int AgeFrom, int AgeUntil);

        List<Person> GetPeopleInCurrentgame(CurrentGame cg);

        List<Person> GetPersons();
    }
}
