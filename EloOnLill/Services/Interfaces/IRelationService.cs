﻿using EloOnLill.Domain;
using EloOnLill.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services.Interfaces
{
    public interface IRelationService
    {
        Relations GetRelation(int ID);

        void AddRelations(Relations relations);

        void AddRelationsManual(string relationName, RelationType relationType);

        void RemoveRelations(Relations relations);

        Relations FindRelationByName(String Relationname);

    }
}
