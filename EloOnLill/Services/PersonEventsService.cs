﻿using EloOnLill.Domain;
using EloOnLill.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services
{
    public class PersonEventsService : BaseService, IPersonEventsInterface
    {
        public PersonEventsService (EloOnLillDbContext2 ctx) : base(ctx) { }


        public void AddToDatabase(PersonEvents r)
        {
            var dbCheck = Db.PersonEvents.Where(p => p.PersonEventsID == r.PersonEventsID || (p.PersonEventsID == r.PersonEventsID && p.Person_PersonID == r.Person_PersonID)).FirstOrDefault();
            var isNew = dbCheck == null;
            if (isNew)
            {

                Db.PersonEvents.Add(r);
            }
            else
            {
                dbCheck = r;
            }
            Db.SaveChanges();
        }

        

        public ObservableCollection<PersonEvents> GetFromDatabase(Person p)
        {
            var PersonEventsList = new List<PersonEvents>();
            PersonEventsList = Db.PersonEvents.Where(x => x.Person_PersonID == p.PersonID).ToList();
            var OB = new ObservableCollection<PersonEvents>();
            foreach (var item in PersonEventsList) {
                OB.Add(item);
            }
            return OB;
        }

        public void RemoveFromDatabase(PersonEvents v)
        {
            throw new NotImplementedException();
        }
    }
}
