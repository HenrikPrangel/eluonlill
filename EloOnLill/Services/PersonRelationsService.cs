﻿using EloOnLill.Domain;
using EloOnLill.Repositories;
using EloOnLill.Repositories.Interfaces;
using EloOnLill.Domain.Enums;
using EloOnLill.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace EloOnLill.Services
{

    /// <summary>
    /// PersonRelations Service.
    /// Contains services relating to PersonRelations class.
    /// </summary>
    /// <remarks>
    /// Can add, remove, update and get all PersonRelations as a list.
    /// </remarks>
    public class PersonRelationsService : BaseService, IPersonRelationsService
    {

        public PersonRelationsService(EloOnLillDbContext2 dbContext2) : base(dbContext2)
        {

        }

        /// <summary>
        /// Adds the input PersonRelations to the DB. Also checks whether handled PersonRelations already exists in the DB in which case it updates handled PersonRelations.
        /// </summary>
        /// <param name="pr"> PersonRelations that needs to be added to the DB</param>
        public void AddPersonRelations(PersonRelations pr)
        {
            var dbCheck = Db.PersonRelations.Where(x => x.PersonRelationsID == pr.PersonRelationsID || (x.Person1_PersonID == pr.Person1_PersonID && x.Person2_PersonID == pr.Person2_PersonID && x.Relation.RelationsID == pr.Relation.RelationsID)).FirstOrDefault();
            var isNew = dbCheck == null;
            if (isNew)
            {
                Db.PersonRelations.Add(pr);
            }
            else
            {
                dbCheck = pr;
            }
            Db.SaveChanges();
        }

        /// <summary>
        /// Allows to create a PersonRelations an then adds it to the DB. Also checks whether handled PersonRelations already exists in the DB in which case it updates handled PersonRelations.
        /// </summary>
        /// <param name="Player"> The first person taking part in the relationship</param>
        /// <param name="person"> The second person taking part in the relationship</param>
        /// <param name="relations"> The relation in which 2 persons are going to be in</param>
        /// <param name="relationLevel"> The relationlevel of the relationship</param>
        public void AddPersonRelationsManual(Person Player, Person person, Relations relations, int relationLevel)
        {
            var pr = new PersonRelations
            {
                Person1 = Player,
                Person2 = person,
                Relation = relations,
                RelationLevel = relationLevel,
                From = DateTime.Now,
                Until = DateTime.Now.AddYears(900)
            };
            AddPersonRelations(pr);
        }

        /// <summary>
        /// Method used in the beginning of the game for the creation of the inital family relationship.
        /// </summary>
        /// <param name="player"> The player person/child of the relation</param>
        /// <param name="father"> The father of the child</param>
        /// <param name="mother"> The mother of the child</param>
        public void Create2ParentFamilyRelation(Person player, Person father, Person mother) //Method used on a new character creation, that creates the initial family
        {
            var y1 = $"p{father.Gender}";
            var y2 = $"p{mother.Gender}";
            var z1 = Db.Relations.Where(x => x.RelationCode.Contains(y1) && x.RelationType == RelationType.Family).FirstOrDefault();
            var z2 = Db.Relations.Where(x => x.RelationCode.Contains(y2) && x.RelationType == RelationType.Family).FirstOrDefault();

            Console.WriteLine(z1);
            Console.WriteLine(z2);
            AddPersonRelationsManual(player, father, z1, 50);
            AddPersonRelationsManual(player, mother, z2, 50);
        }

        /// <summary>
        /// Method that gets all PersonRelations of the input person.
        /// </summary>
        /// <param name="person"> The person whose relationships we are searching for</param>
        /// <returns> A list of personrelations</returns>
        public List<PersonRelations> GetPersonRelations(Person person) //Method that returns all PersonRelations for the input person
        {
            var persRelatList = new List<PersonRelations>();
            persRelatList = Db.PersonRelations.Where(x => x.Person1.PersonID == person.PersonID || x.Person2.PersonID == person.PersonID).ToList();
            return persRelatList;
        }

        /// <summary>
        /// Method that updates all the input relationships in the input list
        /// </summary>
        /// <param name="pr"> The list containing all relationships we want to update</param>
        public void UpdatePersonRelationsList(ObservableCollection<PersonRelations> pr) //Updates the input PersonRelation list
        {
            foreach (var item in pr)
            {
                AddPersonRelations(item);
            }
        }

        /// <summary>
        /// Method that removes the input relationships from the DB
        /// </summary>
        /// <param name="prL"> The list containing all relationships we want to remove</param>
        public void RemoveRelations(List<PersonRelations> prL)
        {
            foreach (var item in prL)
            {
                Db.PersonRelations.Remove(item);
            }
            Db.SaveChanges();
        }
    }
}
