﻿using EloOnLill.Domain;
using EloOnLill.Repositories;
using EloOnLill.Repositories.Interfaces;
using EloOnLill.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services
{
    /// <summary>
    /// Person Service.
    /// Contains services relating to Person class.
    /// </summary>
    /// <remarks>
    /// Can find, add, remove, update and get all Persons as a list.
    /// </remarks>
    public class PersonService : BaseService, IPersonService
    {
        public PersonService(EloOnLillDbContext2 dbContext2) : base(dbContext2)
        {

        }

        /// <summary>
        /// Method that finds a person by the input ID
        /// </summary>
        /// <param name="pr"> ID of the person we are finding</param>
        public Person FindByID(int ID) 
        {
            return Db.People.Where(x => x.PersonID == ID).Single();
        }

        /// <summary>
        /// Method that finds a person by their name
        /// </summary>
        /// <param name="firstname"> Firstname property of the person we want to find</param>
        /// <param name="lastname"> Lastname property of the person we want to find</param>
        public Person FindByName(string firstname, string lastname) 
        {
            return Db.People.Where(x => x.Firstname == firstname && x.Lastname == lastname).FirstOrDefault();
        }

        /// <summary>
        /// Method that adds a person to the DB
        /// </summary>
        /// <param name="person"> The person object, which needs to be added to the DB</param>
        public void AddPerson(Person person) 
        {
            var dbCheck = Db.People.Where(x => x.PersonID == person.PersonID).FirstOrDefault();
            var isNew = dbCheck == null;
            if (isNew)
            {
                person.Birth = DateTime.Now;
                person.Death = DateTime.Now.AddYears(900);
                Db.People.Add(person);
            }
            else
            {
                dbCheck = person;
            }
            Db.SaveChanges();
        }

        /// <summary>
        /// Removes a person from the DB
        /// </summary>
        /// <param name="person"> The person object, which needs to be removed from the DB</param>
        public void RemovePerson(Person person)
        {
            Db.People.Remove(person);
            Db.SaveChanges();
        }

        /// <summary>
        /// Method that creates person with input properties and saves it to the DB
        /// </summary>
        /// <param name="firstname">Property defining the first name of the Person</param>
        /// <param name="lastname">Property defining the last name of the Person</param>
        /// <param name="gender">Property defining the gender of the Person</param>
        /// <param name="age">Property defining the age of the Person</param>
        public void AddPersonManual(string firstname, string lastname, byte gender, int age) 
        {
            var newPerson = new Person
            {
                Firstname = firstname,
                Lastname = lastname,
                Gender = gender,
                Age = age,
                IsAlive = true,
                Birth = DateTime.Now,
                Death = DateTime.Now.AddYears(900)

            };
            AddPerson(newPerson);
        }

        /// <summary>
        /// Method that finds and returns all the people in the DB
        /// </summary>
        /// <returns> A list of all the people in the DB </returns>
        public List<Person> GetPersons() 
        {
            var newPersons = new List<Person>();
            newPersons = Db.People.ToList();
            return newPersons;
        }

        /// <summary>
        /// Method that finds a random person, within the given age group, from the DB
        /// </summary>
        /// <param name="AgeFrom">The minimum age that the random person may have</param>
        /// <param name="AgeUntil">The maximum age that the random person may have</param>
        /// <returns> A person object </returns>
        public Person FindRandomPerson(int AgeFrom, int AgeUntil) //Finds a random person with the input gender and age range
        {
            var y = Db.People.Where(x => x.Age >= AgeFrom && x.Age <= AgeUntil).ToList();
            Random rnd = new Random();
            return y[rnd.Next(0, y.Count() - 1)];
        }

        /// <summary>
        /// Method that updates person info in the DB
        /// </summary>
        /// <param name="persons">The list og people, whose info needs to be updated</param>
        public void UpdatePeople(List<Person> persons) //Update the info of a list of persons
        {
            foreach (var item in persons)
            {
                AddPerson(item);
            }
        }

        /// <summary>
        /// Method that gets all Persons that are tied to a given CurrentGame
        /// </summary>
        /// <param name="cg">The CurrentGame object, from which we want to fiend people tied to it</param>
        public List<Person> GetPeopleInCurrentgame(CurrentGame cg) //Gets all the people from the input Current game
        {
            var persRelatList = Db.PersonRelations.Where(x => x.Person1.PersonID == cg.Person.PersonID || x.Person2.PersonID == cg.Person.PersonID).ToList();
            var personList = new List<Person>();
            foreach (var item in persRelatList)
            {
                if (!personList.Contains(item.Person1))
                {
                    personList.Add(item.Person1);
                }
                if (!personList.Contains(item.Person2))
                {
                    personList.Add(item.Person2);
                }
            }
            return personList;
        }
    }
}
