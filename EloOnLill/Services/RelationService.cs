﻿using EloOnLill.Domain;
using EloOnLill.Domain.Enums;
using EloOnLill.Repositories;
using EloOnLill.Repositories.Interfaces;
using EloOnLill.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EloOnLill.Services
{
    /// <summary>
    /// Relation Service.
    /// Contains services relating to Relation class.
    /// </summary>
    /// <remarks>
    /// Can  add, find, remove and get Relations.
    /// </remarks>
    public class RelationService : BaseService, IRelationService
    {

        public RelationService(EloOnLillDbContext2 dbContext2) : base(dbContext2)
        {

        }

        /// <summary>
        /// Method that adds given Relation to the DB
        /// </summary>
        /// <param name="relations">Relation object that needs to be added to the DB</param>
        public void AddRelations(Relations relations)
        {
            var dbCheck = Db.Relations.Where(x => x.RelationsID == relations.RelationsID || x.RelationName == relations.RelationName).FirstOrDefault();
            var isNew = dbCheck == null;
            if (isNew)
            {
                Db.Relations.Add(relations);
            }
            else
            {
                dbCheck = relations;
            }
            Db.SaveChanges();
        }

        /// <summary>
        /// Method that adds newly created Relation to the DB
        /// </summary>
        /// <param name="relationName">Name of the relation to be created</param>
        /// /// <param name="relationType">Relation type of the relation to be created</param>
        public void AddRelationsManual(string relationName, RelationType relationType)
        {
            var Relation = new Relations
            {
                RelationName = relationName,
                RelationType = relationType,
            };
            AddRelations(Relation);
        }

        /// <summary>
        /// Method that finds a Relation from the DB, by its name
        /// </summary>
        /// <param name="Relationname">Name of the relation that needs to be found</param>
        public Relations FindRelationByName(string Relationname)
        {
            return Db.Relations.Where(x => x.RelationName == Relationname).FirstOrDefault();
        }

        /// <summary>
        /// Method that finds a Relation from the DB, by its ID
        /// </summary>
        /// <param name="ID">ID of the relation that needs to be found</param>
        public Relations GetRelation(int ID)
        {
            Relations relation = Db.Relations.Find(ID);
            return relation;
        }

        /// <summary>
        /// Method that removes a Relation from the DB
        /// </summary>
        /// <param name="relations">Relation object, that needs to be removed</param>
        public void RemoveRelations(Relations relations)
        {
            Db.Relations.Remove(relations);
        }
    }
}
